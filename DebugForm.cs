﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace ValgService2009
{
    /// <summary>
    /// Simple Win-form to aid local debugging of the service
    /// </summary>
    public partial class DebugForm : Form
    {
        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(DebugForm));

        /// <summary>
        /// The actual main work object
        /// </summary>
        protected QueryComponent queryObject = new QueryComponent();

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugForm"/> class.
        /// </summary>
        public DebugForm()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Load event of the DebugForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void DebugForm_Load(object sender, EventArgs e)
        {
            queryObject.Init();
        }

        /// <summary>
        /// Handles the FormClosing event of the DebugForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
        private void DebugForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Info("ValgService2009 DEBUGMODE starting");
                queryObject.Start();
                logger.Info("ValgService2009 DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Error("ValgService2009 DEBUGMODE DID NOT START properly", ex);
                queryObject.Stop();
            }
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            queryObject.Stop();
            logger.Info("ValgService2009 DEBUGMODE stopped");
        }

        private void saveCacheBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValgUtils.URLCache.Count == 0) throw new Exception("URL cache is empty.");
                
                var list = new List<string>();
                foreach (string key in ValgUtils.URLCache.Keys)
                {
                    list.Add(String.Format("{0}|{1}", key, ValgUtils.URLCache[key]));
                }
                System.IO.File.WriteAllLines(cacheFile.Text, list.ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error saving URL cache: " + ex.Message);
            }
        }

        private void loadCacheBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ValgUtils.URLCache.Clear();
                foreach (string line in System.IO.File.ReadAllLines(cacheFile.Text))
                {
                    var ln = line.Split('|');
                    ValgUtils.URLCache.Add(ln[0], ln[1]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading URL cache: " + ex.Message);
            }
        }

        private void downloadCacheBtn_Click(object sender, EventArgs e)
        {
            try
            {
                log4net.NDC.Push("URLCacheDump");
                if (ValgUtils.URLCache.Count == 0) throw new Exception("URL cache is empty.");

                foreach (string key in ValgUtils.URLCache.Keys)
                {
                    var res = ValgUtils.DoQuery(ValgUtils.URLCache[key]);
                    var xml = ValgUtils.GetXmlDocument(res);
                    ValgUtils.DumpToFile(xml, cacheOutput.Text, key.ToUpper());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error downloading the URL cache content: " + ex.Message);
            }
            log4net.NDC.Pop();

        }


    }
}
