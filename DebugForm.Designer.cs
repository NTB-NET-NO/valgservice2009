﻿namespace ValgService2009
{
    partial class DebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startBtn = new System.Windows.Forms.Button();
            this.stopBtn = new System.Windows.Forms.Button();
            this.saveCacheBtn = new System.Windows.Forms.Button();
            this.loadCacheBtn = new System.Windows.Forms.Button();
            this.downloadCacheBtn = new System.Windows.Forms.Button();
            this.cacheFile = new System.Windows.Forms.TextBox();
            this.cacheOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(23, 66);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(156, 42);
            this.startBtn.TabIndex = 0;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Location = new System.Drawing.Point(185, 66);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(155, 43);
            this.stopBtn.TabIndex = 1;
            this.stopBtn.Text = "Stop";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // saveCacheBtn
            // 
            this.saveCacheBtn.Location = new System.Drawing.Point(23, 114);
            this.saveCacheBtn.Name = "saveCacheBtn";
            this.saveCacheBtn.Size = new System.Drawing.Size(156, 47);
            this.saveCacheBtn.TabIndex = 2;
            this.saveCacheBtn.Text = "Save URL cache";
            this.saveCacheBtn.UseVisualStyleBackColor = true;
            this.saveCacheBtn.Click += new System.EventHandler(this.saveCacheBtn_Click);
            // 
            // loadCacheBtn
            // 
            this.loadCacheBtn.Location = new System.Drawing.Point(185, 114);
            this.loadCacheBtn.Name = "loadCacheBtn";
            this.loadCacheBtn.Size = new System.Drawing.Size(155, 46);
            this.loadCacheBtn.TabIndex = 3;
            this.loadCacheBtn.Text = "Load URL Cache";
            this.loadCacheBtn.UseVisualStyleBackColor = true;
            this.loadCacheBtn.Click += new System.EventHandler(this.loadCacheBtn_Click);
            // 
            // downloadCacheBtn
            // 
            this.downloadCacheBtn.Location = new System.Drawing.Point(23, 226);
            this.downloadCacheBtn.Name = "downloadCacheBtn";
            this.downloadCacheBtn.Size = new System.Drawing.Size(301, 48);
            this.downloadCacheBtn.TabIndex = 4;
            this.downloadCacheBtn.Text = "Download from URL cache";
            this.downloadCacheBtn.UseVisualStyleBackColor = true;
            this.downloadCacheBtn.Click += new System.EventHandler(this.downloadCacheBtn_Click);
            // 
            // cacheFile
            // 
            this.cacheFile.Location = new System.Drawing.Point(23, 181);
            this.cacheFile.Name = "cacheFile";
            this.cacheFile.Size = new System.Drawing.Size(391, 26);
            this.cacheFile.TabIndex = 5;
            this.cacheFile.Text = "D:\\Jobb\\NTB\\valg\\urls.cache";
            // 
            // cacheOutput
            // 
            this.cacheOutput.Location = new System.Drawing.Point(23, 292);
            this.cacheOutput.Name = "cacheOutput";
            this.cacheOutput.Size = new System.Drawing.Size(391, 26);
            this.cacheOutput.TabIndex = 6;
            this.cacheOutput.Text = "D:\\Jobb\\NTB\\valg\\cachedump";
            // 
            // DebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 406);
            this.Controls.Add(this.cacheOutput);
            this.Controls.Add(this.cacheFile);
            this.Controls.Add(this.downloadCacheBtn);
            this.Controls.Add(this.loadCacheBtn);
            this.Controls.Add(this.saveCacheBtn);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.startBtn);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DebugForm";
            this.Text = "DebugForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DebugForm_FormClosing);
            this.Load += new System.EventHandler(this.DebugForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button saveCacheBtn;
        private System.Windows.Forms.Button loadCacheBtn;
        private System.Windows.Forms.Button downloadCacheBtn;
        private System.Windows.Forms.TextBox cacheFile;
        private System.Windows.Forms.TextBox cacheOutput;
    }
}