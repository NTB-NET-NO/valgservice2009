﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using log4net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Specialized;


namespace ValgService2009
{
    /// <summary>
    /// Generic valg-related exception
    /// </summary>
    public class ValgException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValgException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public ValgException(string message)
            : base (message)
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="ValgException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public ValgException(string message, Exception exception)
            : base(message,exception)
        {}
        
    }


    /// <summary>
    /// WinForm used when debugging the service
    /// </summary>
    public class ValgUtils
    {
        /// <summary>
        /// Log object
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(ValgUtils));

        /// <summary>
        /// Resource stream to read empty XML report
        /// </summary>
        protected static Stream emptyXmlStream;

        /// <summary>
        /// Collection of fewtched URLs
        /// </summary>
        public static StringDictionary URLCache;

        /// <summary>
        /// Initializes the <see cref="ValgUtils"/> class.
        /// </summary>
        static ValgUtils()
        {
            //Configure logger
            log4net.Config.XmlConfigurator.Configure();
            
            //Prepare empty XML document stream from resource
            emptyXmlStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ValgService2009.Resources.empty-response.xml");

            //Clean URL cache
            URLCache = new StringDictionary();
        }

        /// <summary>
        /// Gets the empty XML doc from resource
        /// </summary>
        /// <returns></returns>
        public static XmlDocument GetEmptyResponseXML()
        {
            //Move to start
            emptyXmlStream.Seek(0, SeekOrigin.Begin);

            //Load
            XmlDocument ret = new XmlDocument();
            ret.Load(emptyXmlStream);
            
            //Return
            return ret;
        }

        /// <summary>
        /// Sends the query to the server and returns the fetched XML.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public static string DoQuery(string query)
        {
            string ret = "";

            //Create the URL
            string url = QueryComponent.server + query;
            logger.Debug("DoQuery request: " + url);

            //Create the HTTP request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = QueryComponent.timeout;

            //Load Cert
            if (QueryComponent.certificate != null)
            {
                request.ClientCertificates.Add(QueryComponent.certificate);
            }
            //Get the HTTP response with the resulting data
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(QueryComponent.server_charset));
                ret = reader.ReadToEnd();
                reader.Close();
                logger.Debug("DoQuery request completed OK");
            }
            catch (WebException webEx)
            {
                logger.Error("HTTP request failed: " + webEx.Message);
                if (webEx.Response != null) webEx.Response.Close();
            }
            catch (Exception ex)
            {
                logger.Error("General request error: " + ex.Message, ex);
            }

            return ret;
        }

        /// <summary>
        /// Creates the REST R06 query.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static string CreateRESTR06Query(int index)
        {
            string ret = String.Format("/R06/{0}/XML", index);
            logger.DebugFormat("R06 query string created: {0}", ret);
            return ret;
        }

        /// <summary>
        /// Creates the REST query.
        /// </summary>
        /// <param name="rapport">The rapport.</param>
        /// <param name="omraade">The omraade.</param>
        /// <param name="krets">The krets.</param>
        /// <returns></returns>
        public static string CreateRESTQuery(string rapport, string omraade, string krets)
        {
            //Basic query
            string ret = "/" + rapport;

            //Add primary area code
            if ((rapport == "ST02" ||
                 rapport == "ST03" ||
                 rapport == "ST04" ||
                 rapport == "ST05" ||
                 rapport == "F01" ||
                 rapport == "F02" ||
                 rapport == "F03" ||
                 rapport == "F04" ||
                 rapport == "K01" ||
                 rapport == "K02" ||
                 rapport == "K03" ||
                 rapport == "K04" ||
                 rapport == "K08") && !String.IsNullOrEmpty(omraade))
            {
                ret += "/" + omraade;
            }

            //Add secondary
            if ((rapport == "ST03" ||
                 rapport == "F03" ||
                 rapport == "K03") && !String.IsNullOrEmpty(krets))
            {
                ret += "/" + krets;
            }

            ret += "/XML";
            logger.DebugFormat("Report query string created: {0}", ret);
            return ret;
        }

        /// <summary>
        /// Loads XML and checks for error messages. Wraps reports in response-nodes.
        /// Exception is thrown on any errors, otherwise a valid XmlDocument is returned
        /// </summary>
        /// <param name="content">The content.</param>
        /// <returns>Valid XmlDocument</returns>
        public static XmlDocument GetXmlDocument(String content)
        {
            //Check for emtpy content
            if (String.IsNullOrEmpty(content))
                throw new ValgException("Empty XML report content.");

            //Try to load content
            XmlDocument tmp = new XmlDocument();
            tmp.LoadXml(content);

            //Look for error message
            XmlNode nd = tmp.SelectSingleNode("//data[@navn='feilmelding']");
            if (nd != null)
            {
                throw new ValgException("Report error message: " + nd.InnerText);
            }

            //Wrap result in empty container envelope xml-doc
            XmlDocument ret = null;
            try
            {
                ret = ValgUtils.GetEmptyResponseXML();
                nd = ret.ImportNode(tmp.DocumentElement, true);
                ret.DocumentElement.AppendChild(nd);

            }
            catch (Exception ex)
            {
                throw new ValgException("Error wrapping XML report into envelope doc.", ex);
            }

            ////Fix RV -> R
            //try
            //{
            //    XmlNodeList nl = ret.SelectNodes("//data[@navn='Partikode' and . = 'RV']");
            //    foreach (XmlNode n in nl)
            //    {
            //        n.InnerText = "R";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new ValgException("Error converting party name RV to R.", ex);
            //}

            ////Fix MDG -> kategori 1
            //try
            //{
            //    XmlNodeList nl = ret.SelectNodes("//liste[data[@navn='Partikode' and . = 'MDG']]/data[@navn='Partikategori']");
            //    foreach (XmlNode n in nl)
            //    {
            //        n.InnerText = "1";
            //    }
            //    nl = ret.SelectNodes("//liste[data[@navn='PartikodeL' and . = 'MDG']]/data[@navn='PartikategoriL']");
            //    foreach (XmlNode n in nl)
            //    {
            //        n.InnerText = "1";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new ValgException("Error converting category 1 for MDG.", ex);
            //}

            //And return complete document 
            return ret;
        }


        /// <summary>
        /// Dumps xml content to file.
        /// </summary>
        /// <param name="XML">The XML.</param>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        public static void DumpToFile(string XML, string path, string filename)
        {
            //Handle directory structure
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                logger.DebugFormat("Directory created: {0}", path);
            }

            //Dump the data
            StreamWriter wrt = new StreamWriter(Path.Combine(path, filename), false, Encoding.GetEncoding(QueryComponent.file_charset));
            wrt.WriteLine(XML);
            wrt.Close();
            wrt = null;
            logger.Debug("Saved " + Path.Combine(path, filename) + " to disk.");
        }

        /// <summary>
        /// Dumps xml content to file.
        /// </summary>
        /// <param name="xmldoc">The xmldoc.</param>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        public static void DumpToFile(XmlDocument xmldoc, string path, string filename)
        {
            //Handle directory structure
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                logger.DebugFormat("Directory created: {0}", path);
            }

            //Dump the data
            XmlTextWriter w = new XmlTextWriter(Path.Combine(path, filename), Encoding.GetEncoding(QueryComponent.file_charset));
            w.Formatting = Formatting.Indented;
            w.Indentation = 2;
            w.IndentChar = ' ';
            xmldoc.Save(w);
            w.Close();

            logger.Debug("Saved " + Path.Combine(path, filename) + " to disk.");
        }

        public static void UpdateURLCache(string contentURL, string filename)
        {
            URLCache[filename] = contentURL;           
        }

       /// <summary>
        /// Returns the correct report prefix letter. Acconts for differing valgtype and report prefix for K and F
        /// </summary>
        /// <param name="valg">The valg.</param>
        /// <returns></returns>
        public static string GetReportPrefix(string valg)
        {
            //Differing report ids between election types
            if (valg == "KO") return "K";
            else if (valg == "FY") return "F";

            return valg;
        }

        /// <summary>
        /// Returns the proper bydels-number for Oslo, handles "Marka"
        /// </summary>
        /// <param name="kretsNr">The krets nr.</param>
        public static string GetBydelsNumber(string kretsNr)
        {
            //"Marka" is a special case
            if (kretsNr == "1701") return "07"; //Sørkedalen -> Vestre aker
            else if (kretsNr == "1702") return "08"; //Maridalen -> Nordre aker

            // "Rådhuset" needs mapping
            else if (kretsNr == "1601") return "04"; //Rådhuset/Sentrum -> St. Hanshaugen

            // Techincal, i.e. pre-election votes
            else if (kretsNr == "VIRT") return "00"; //Virtual pre-election votes for krets/bydel

            //Otherwise we use the first two digits
            else return kretsNr.Substring(0, 2);
        }

        /// <summary>
        /// Returns the proper report for Bydeler for Oslo
        /// </summary>
        /// <param name="valg">The valg.</param>
        /// <returns></returns>
        public static string GetBydelsReport(string valg)
        {
            //Differing report ids between election types
            if (valg == "ST") return "ST05";
            else if (valg == "K") return "K08";

            //Not applicable for valgtype FY
                            
            return string.Empty;
        }


    }
}
