﻿namespace ValgService2009
{
    partial class QueryComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pollTimer = new System.Timers.Timer();
            this.maintenanceTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).BeginInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 10000;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            // 
            // maintenanceTimer
            // 
            this.maintenanceTimer.Interval = 120000;
            this.maintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.maintenanceTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maintenanceTimer)).EndInit();

        }

        #endregion

        private System.Timers.Timer pollTimer;
        private System.Timers.Timer maintenanceTimer;
    }
}
