﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using log4net;
using System.Security.Cryptography.X509Certificates;

namespace ValgService2009
{
    /// <summary>
    /// Defines report aggregate types
    /// </summary>
    public enum AggregateTypes
    {
        /// <summary>
        /// Specifies aggregation of ST03-reports (kretser, stortingsvalg)
        /// </summary>
        ST03,
        /// <summary>
        /// Specifies aggregation of ST04-reports (fylker, stortingsvalg)
        /// </summary>
        ST04,
        /// <summary>
        /// Specifies aggregation of ST05-reports (bydeler, Oslo, stortingsvalg)
        /// </summary>
        ST05,
        /// <summary>
        /// Specifies aggregation of F01-reports (kretser, fylkestingsvalg)
        /// </summary>
        F01,
        /// <summary>
        /// Specifies aggregation of F03-reports (kretser, fylkestingsvalg)
        /// </summary>
        F03,
        /// <summary>
        /// Specifies aggregation of F04-reports (fylker, fylkestingsvalg)
        /// </summary>
        F04,
        /// <summary>
        /// Specifies aggregation of K03-reports (kretser, kommunevalg)
        /// </summary>
        K01,
        /// <summary>
        /// Specifies aggregation of K04-reports (fylker, kommunevalg)
        /// </summary>
        K03,
        /// <summary>
        /// Specifies aggregation of K04-reports (fylker, kommunevalg)
        /// </summary>
        K04,
        /// <summary>
        /// Specifies aggregation of K08-reports (bydeler, Oslo, kommunevalg)
        /// </summary>
        K08
    }

    /// <summary>
    /// Defines Valg types
    /// </summary>
    public enum ValgTypes
    {
        /// <summary>
        /// Specifies Stortingsvalg
        /// </summary>
        ST,
        /// <summary>
        /// Specifies Kommune/fylkestingsvalg
        /// </summary>
        FK
    }

    /// <summary>
    /// Main work object. Uses a polltimer and does the actual work. Object used with both service mode and debug mode
    /// </summary>
    public partial class QueryComponent : Component
    {
        /// <summary>
        /// Struct placeholder for an out of sync report item
        /// </summary>
        protected struct SyncItem
        {
            /// <summary>
            /// Valgtype
            /// </summary>
            public String valg;
            /// <summary>
            /// Report name
            /// </summary>
            public String report;

            /// <summary>
            /// Fylke
            /// </summary>
            public String fylkeNr;
            /// <summary>
            /// Fylkenavn
            /// </summary>
            public String fylke;
            /// <summary>
            /// Kommune
            /// </summary>
            public String kommuneNr;
            /// <summary>
            /// Kommunenavn
            /// </summary>
            public String kommune;
            /// <summary>
            /// Krets
            /// </summary>
            public String kretsNr;
            /// <summary>
            /// Krets name
            /// </summary>
            public String krets;

            /// <summary>
            /// Expected statusindicator
            /// </summary>
            public String expectedStatus;
            /// <summary>
            /// Actuall status indicator
            /// </summary>
            public String actuallStatus;

            /// <summary>
            /// Timestamp for when the async was detected
            /// </summary>
            public DateTime timestamp;
        }

        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(QueryService));

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryComponent"/> class.
        /// </summary>
        public QueryComponent()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryComponent"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public QueryComponent(IContainer container)
        {
            container.Add(this);
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }


        #region Class variables

        /// <summary>
        /// Data log folder
        /// </summary>
        protected string queryLogFolder;

        /// <summary>
        /// Temp folder for aggregated reports
        /// </summary>
        protected string tempFolder;

        /// <summary>
        /// Main output folder
        /// </summary>
        protected string xmlOutFolder;

        /// <summary>
        /// Valg type
        /// </summary>
        protected ValgTypes valgType;

        /// <summary>
        /// HTTP request server, base URL
        /// </summary>
        public static string server = string.Empty;

        /// <summary>
        /// HTTP client certificate
        /// </summary>
        public static X509Certificate certificate = null;

        /// <summary>
        /// Expected server-report charset
        /// </summary>
        public static string server_charset = "iso-8859-1";

        /// <summary>
        /// Local file-report charset
        /// </summary>
        public static string file_charset = "iso-8859-1";

        /// <summary>
        /// HTTP request timeout
        /// </summary>
        public static int timeout = 0;

        /// <summary>
        /// Query interval
        /// </summary>
        protected int interval = 0;

        /// <summary>
        /// Maintenance interval
        /// </summary>
        protected int mInterval = 0;

        /// <summary>
        /// Simple sync
        /// </summary>
        protected bool enableSimpleSync = false;

        /// <summary>
        /// Simple sync interval
        /// </summary>
        protected int simpleSyncInterval = 0;

        /// <summary>
        /// Last simple sync 
        /// </summary>
        protected DateTime lastSimpleSync = DateTime.MinValue;

        /// <summary>
        /// Advanced sync
        /// </summary>
        protected bool enableAdvancedSync = false;

        /// <summary>
        /// Max allowed async
        /// </summary>
        protected int maxAsync = 0;

        /// <summary>
        /// Query interval
        /// </summary>
        protected int waitOnStop = 0;

        /// <summary>
        /// Query interval
        /// </summary>
        protected bool lastnodeCheck = false;

        /// <summary>
        /// Current R06 report id (Løpenummer)
        /// </summary>
        int index = 0;

        /// <summary>
        /// Dirty-flag for ST06 refetch
        /// </summary>
        bool dirtyST06 = false;

        /// <summary>
        /// Exit control event
        /// </summary>
        protected System.Threading.AutoResetEvent _busyEvent = new System.Threading.AutoResetEvent(true);

        //Other
        /// <summary>
        /// Dictionary for storing main reports to be fetched
        /// </summary>
        protected StringDictionary reportFetchFlags;

        /// <summary>
        /// Dictionary for storing fylker to avoid duplicate downloads at each run
        /// </summary>
        protected StringCollection fetchedFylker;

        /// <summary>
        /// Dictionary for storing kommuner to avoid duplicate downloads at each run
        /// </summary>
        protected StringCollection fetchedKommuner;

        /// <summary>
        /// Dictionary for storing bydeler to avoid duplicate downloads at each run
        /// </summary>
        protected StringCollection fetchedBydeler;

        /// <summary>
        /// Reusable XML docs for parsing results
        /// </summary>
        protected XmlDocument XMLDoc, XMLDocTmp;

        /// <summary>
        /// Mapped sync errors per fylke. Looked up by fylkesnummer-kommunenummer-kretsnummer
        /// </summary>
        protected Dictionary<String, Dictionary<String, Dictionary<String, SyncItem>>> syncItems_ST04 = new Dictionary<string, Dictionary<string, Dictionary<string, SyncItem>>>();

        /// <summary>
        /// Override max expected status values for certain areas.
        /// </summary>
        protected Dictionary<String, Dictionary<String, String>> overrideStatusValues = new Dictionary<string, Dictionary<string, String>>();


        #endregion

        //Load settings and initiate server
        /// <summary>
        /// Inits this instance.
        /// </summary>
        public void Init()
        {
            //Log context
            NDC.Push("INIT");

            #region Sync status overrides
            //Init overridable max status values
            //Oslo
            overrideStatusValues["0301"] = new Dictionary<string, string>();
            overrideStatusValues["0301"]["VIRT"] = "8";
            overrideStatusValues["0301"]["0101"] = "4";
            overrideStatusValues["0301"]["0102"] = "4";
            overrideStatusValues["0301"]["0103"] = "4";
            overrideStatusValues["0301"]["0104"] = "4";
            overrideStatusValues["0301"]["0105"] = "4";
            overrideStatusValues["0301"]["0106"] = "4";
            overrideStatusValues["0301"]["0107"] = "4";
            overrideStatusValues["0301"]["0108"] = "4";

            overrideStatusValues["0301"]["0201"] = "4";
            overrideStatusValues["0301"]["0202"] = "4";
            overrideStatusValues["0301"]["0203"] = "4";
            overrideStatusValues["0301"]["0204"] = "4";
            overrideStatusValues["0301"]["0205"] = "4";
            overrideStatusValues["0301"]["0206"] = "4";

            overrideStatusValues["0301"]["0301"] = "4";
            overrideStatusValues["0301"]["0302"] = "4";
            overrideStatusValues["0301"]["0303"] = "4";
            overrideStatusValues["0301"]["0304"] = "4";
            overrideStatusValues["0301"]["0305"] = "4";
            overrideStatusValues["0301"]["0306"] = "4";

            overrideStatusValues["0301"]["0401"] = "4";
            overrideStatusValues["0301"]["0402"] = "4";
            overrideStatusValues["0301"]["0403"] = "4";
            overrideStatusValues["0301"]["0404"] = "4";
            overrideStatusValues["0301"]["0405"] = "4";
            overrideStatusValues["0301"]["0406"] = "4";

            overrideStatusValues["0301"]["0501"] = "4";
            overrideStatusValues["0301"]["0502"] = "4";
            overrideStatusValues["0301"]["0503"] = "4";
            overrideStatusValues["0301"]["0504"] = "4";
            overrideStatusValues["0301"]["0505"] = "4";
            overrideStatusValues["0301"]["0506"] = "4";
            overrideStatusValues["0301"]["0507"] = "4";
            overrideStatusValues["0301"]["0508"] = "4";
            overrideStatusValues["0301"]["0509"] = "4";
            overrideStatusValues["0301"]["0510"] = "4";

            overrideStatusValues["0301"]["0601"] = "4";
            overrideStatusValues["0301"]["0602"] = "4";
            overrideStatusValues["0301"]["0603"] = "4";
            overrideStatusValues["0301"]["0604"] = "4";
            overrideStatusValues["0301"]["0605"] = "4";
            overrideStatusValues["0301"]["0606"] = "4";

            overrideStatusValues["0301"]["0701"] = "4";
            overrideStatusValues["0301"]["0702"] = "4";
            overrideStatusValues["0301"]["0703"] = "4";
            overrideStatusValues["0301"]["0704"] = "4";
            overrideStatusValues["0301"]["0705"] = "4";
            overrideStatusValues["0301"]["0706"] = "4";
            overrideStatusValues["0301"]["0707"] = "4";
            overrideStatusValues["0301"]["0708"] = "4";
            overrideStatusValues["0301"]["0709"] = "4";

            overrideStatusValues["0301"]["0801"] = "4";
            overrideStatusValues["0301"]["0802"] = "4";
            overrideStatusValues["0301"]["0803"] = "4";
            overrideStatusValues["0301"]["0804"] = "4";
            overrideStatusValues["0301"]["0805"] = "4";
            overrideStatusValues["0301"]["0806"] = "4";
            overrideStatusValues["0301"]["0807"] = "4";
            overrideStatusValues["0301"]["0808"] = "4";
            overrideStatusValues["0301"]["0809"] = "4";
            overrideStatusValues["0301"]["0810"] = "4";

            overrideStatusValues["0301"]["0901"] = "4";
            overrideStatusValues["0301"]["0902"] = "4";
            overrideStatusValues["0301"]["0903"] = "4";
            overrideStatusValues["0301"]["0904"] = "4";
            overrideStatusValues["0301"]["0905"] = "4";

            overrideStatusValues["0301"]["1001"] = "4";
            overrideStatusValues["0301"]["1002"] = "4";
            overrideStatusValues["0301"]["1003"] = "4";
            overrideStatusValues["0301"]["1004"] = "4";

            overrideStatusValues["0301"]["1101"] = "4";
            overrideStatusValues["0301"]["1102"] = "4";
            overrideStatusValues["0301"]["1103"] = "4";
            overrideStatusValues["0301"]["1104"] = "4";
            overrideStatusValues["0301"]["1105"] = "4";

            overrideStatusValues["0301"]["1201"] = "4";
            overrideStatusValues["0301"]["1202"] = "4";
            overrideStatusValues["0301"]["1203"] = "4";
            overrideStatusValues["0301"]["1204"] = "4";
            overrideStatusValues["0301"]["1205"] = "4";
            overrideStatusValues["0301"]["1206"] = "4";
            overrideStatusValues["0301"]["1207"] = "4";

            overrideStatusValues["0301"]["1301"] = "4";
            overrideStatusValues["0301"]["1302"] = "4";
            overrideStatusValues["0301"]["1303"] = "4";
            overrideStatusValues["0301"]["1304"] = "4";
            overrideStatusValues["0301"]["1305"] = "4";
            overrideStatusValues["0301"]["1306"] = "4";
            overrideStatusValues["0301"]["1307"] = "4";
            overrideStatusValues["0301"]["1308"] = "4";
            overrideStatusValues["0301"]["1309"] = "4";
            overrideStatusValues["0301"]["1310"] = "4";

            overrideStatusValues["0301"]["1401"] = "4";
            overrideStatusValues["0301"]["1402"] = "4";
            overrideStatusValues["0301"]["1403"] = "4";
            overrideStatusValues["0301"]["1404"] = "4";
            overrideStatusValues["0301"]["1405"] = "4";
            overrideStatusValues["0301"]["1406"] = "4";
            overrideStatusValues["0301"]["1407"] = "4";
            overrideStatusValues["0301"]["1408"] = "4";
            overrideStatusValues["0301"]["1409"] = "4";

            overrideStatusValues["0301"]["1501"] = "4";
            overrideStatusValues["0301"]["1502"] = "4";
            overrideStatusValues["0301"]["1503"] = "4";
            overrideStatusValues["0301"]["1504"] = "4";
            overrideStatusValues["0301"]["1505"] = "4";
            overrideStatusValues["0301"]["1506"] = "4";
            overrideStatusValues["0301"]["1507"] = "4";

            overrideStatusValues["0301"]["1601"] = "4";

            overrideStatusValues["0301"]["1701"] = "4";
            overrideStatusValues["0301"]["1702"] = "4";

            //Stavanger
            overrideStatusValues["1103"] = new Dictionary<string, string>();
            overrideStatusValues["1103"]["VIRT"] = "8";
            overrideStatusValues["1103"]["0001"] = "4";
            overrideStatusValues["1103"]["0004"] = "4";
            overrideStatusValues["1103"]["0005"] = "4";
            overrideStatusValues["1103"]["0006"] = "4";
            overrideStatusValues["1103"]["0007"] = "4";
            overrideStatusValues["1103"]["0008"] = "4";
            overrideStatusValues["1103"]["0009"] = "4";
            overrideStatusValues["1103"]["0011"] = "4";
            overrideStatusValues["1103"]["0013"] = "4";
            overrideStatusValues["1103"]["0014"] = "4";
            overrideStatusValues["1103"]["0016"] = "4";
            overrideStatusValues["1103"]["0017"] = "4";
            overrideStatusValues["1103"]["0018"] = "4";
            overrideStatusValues["1103"]["0019"] = "4";
            overrideStatusValues["1103"]["0020"] = "4";
            overrideStatusValues["1103"]["0021"] = "4";
            overrideStatusValues["1103"]["0022"] = "4";
            overrideStatusValues["1103"]["0023"] = "4";
            overrideStatusValues["1103"]["0024"] = "4";
            overrideStatusValues["1103"]["0025"] = "4";
            overrideStatusValues["1103"]["0026"] = "4";
            overrideStatusValues["1103"]["0027"] = "4";
            
            //Bergen
            overrideStatusValues["1201"] = new Dictionary<string, string>();
            overrideStatusValues["1201"]["VIRT"] = "8";
            overrideStatusValues["1201"]["0001"] = "4";
            overrideStatusValues["1201"]["0002"] = "4";
            overrideStatusValues["1201"]["0003"] = "4";
            overrideStatusValues["1201"]["0004"] = "4";
            overrideStatusValues["1201"]["0007"] = "4";
            overrideStatusValues["1201"]["0009"] = "4";
            overrideStatusValues["1201"]["0013"] = "4";
            overrideStatusValues["1201"]["0014"] = "4";
            overrideStatusValues["1201"]["0015"] = "4";
            overrideStatusValues["1201"]["0018"] = "4";
            overrideStatusValues["1201"]["0019"] = "4";
            overrideStatusValues["1201"]["0022"] = "4";
            overrideStatusValues["1201"]["0023"] = "4";
            overrideStatusValues["1201"]["0024"] = "4";
            overrideStatusValues["1201"]["0025"] = "4";
            overrideStatusValues["1201"]["0028"] = "4";
            overrideStatusValues["1201"]["0029"] = "4";
            overrideStatusValues["1201"]["0030"] = "4";
            overrideStatusValues["1201"]["0031"] = "4";
            overrideStatusValues["1201"]["0032"] = "4";
            overrideStatusValues["1201"]["0035"] = "4";
            overrideStatusValues["1201"]["0036"] = "4";
            overrideStatusValues["1201"]["0037"] = "4";
            overrideStatusValues["1201"]["0039"] = "4";
            overrideStatusValues["1201"]["0040"] = "4";
            overrideStatusValues["1201"]["0044"] = "4";
            overrideStatusValues["1201"]["0045"] = "4";
            overrideStatusValues["1201"]["0048"] = "4";
            overrideStatusValues["1201"]["0049"] = "4";
            overrideStatusValues["1201"]["0051"] = "4";
            overrideStatusValues["1201"]["0052"] = "4";
            overrideStatusValues["1201"]["0054"] = "4";
            overrideStatusValues["1201"]["0055"] = "4";
            overrideStatusValues["1201"]["0056"] = "4";
            overrideStatusValues["1201"]["0057"] = "4";
            overrideStatusValues["1201"]["0058"] = "4";
            overrideStatusValues["1201"]["0059"] = "4";
            overrideStatusValues["1201"]["0061"] = "4";
            overrideStatusValues["1201"]["0062"] = "4";
            overrideStatusValues["1201"]["0063"] = "4";
            overrideStatusValues["1201"]["0067"] = "4";
            overrideStatusValues["1201"]["0068"] = "4";
            overrideStatusValues["1201"]["0069"] = "4";
            overrideStatusValues["1201"]["0070"] = "4";
            overrideStatusValues["1201"]["0071"] = "4";
            overrideStatusValues["1201"]["0074"] = "4";
            overrideStatusValues["1201"]["0076"] = "4";
            overrideStatusValues["1201"]["0077"] = "4";
            //overrideStatusValues["1201"]["FREM"] = "4";
            //overrideStatusValues["1201"]["VALG"] = "4";

            //Trondheim
            overrideStatusValues["1601"] = new Dictionary<string, string>();
            overrideStatusValues["1601"]["VIRT"] = "8";
            overrideStatusValues["1601"]["0001"] = "4";
            overrideStatusValues["1601"]["0002"] = "4";
            overrideStatusValues["1601"]["0003"] = "4";
            overrideStatusValues["1601"]["0004"] = "4";
            overrideStatusValues["1601"]["0005"] = "4";
            overrideStatusValues["1601"]["0006"] = "4";
            overrideStatusValues["1601"]["0007"] = "4";
            overrideStatusValues["1601"]["0008"] = "4";
            overrideStatusValues["1601"]["0009"] = "4";
            overrideStatusValues["1601"]["0010"] = "4";
            overrideStatusValues["1601"]["0011"] = "4";
            overrideStatusValues["1601"]["0012"] = "4";
            overrideStatusValues["1601"]["0013"] = "4";
            overrideStatusValues["1601"]["0014"] = "4";
            overrideStatusValues["1601"]["0015"] = "4";
            overrideStatusValues["1601"]["0016"] = "4";
            overrideStatusValues["1601"]["0017"] = "4";
            overrideStatusValues["1601"]["0018"] = "4";
            overrideStatusValues["1601"]["0019"] = "4";
            overrideStatusValues["1601"]["0020"] = "4";
            overrideStatusValues["1601"]["0021"] = "4";
            overrideStatusValues["1601"]["0022"] = "4";
            overrideStatusValues["1601"]["0023"] = "4";
            overrideStatusValues["1601"]["0024"] = "4";
            overrideStatusValues["1601"]["0025"] = "4";
            overrideStatusValues["1601"]["0026"] = "4";
            overrideStatusValues["1601"]["0027"] = "4";
            overrideStatusValues["1601"]["0028"] = "4";
            overrideStatusValues["1601"]["0029"] = "4";
            overrideStatusValues["1601"]["0030"] = "4";
            overrideStatusValues["1601"]["0031"] = "4";
            overrideStatusValues["1601"]["0032"] = "4";
            overrideStatusValues["1601"]["0033"] = "4";
            overrideStatusValues["1601"]["0034"] = "4";
            overrideStatusValues["1601"]["0035"] = "4";
            overrideStatusValues["1601"]["0036"] = "4";
            overrideStatusValues["1601"]["0037"] = "4";
            overrideStatusValues["1601"]["0038"] = "4";
            overrideStatusValues["1601"]["0039"] = "4";
            #endregion

            //Init vars
            XMLDoc = new XmlDocument();
            XMLDocTmp = new XmlDocument();
            fetchedKommuner = new StringCollection();
            fetchedFylker = new StringCollection();
            fetchedBydeler = new StringCollection();
            reportFetchFlags = new StringDictionary();

            try
            {

                //Load settings
                queryLogFolder = ConfigurationManager.AppSettings["queryLogFolder"];
                tempFolder = ConfigurationManager.AppSettings["tempFolder"];
                xmlOutFolder = ConfigurationManager.AppSettings["xmlOutFolder"];
                valgType = (ValgTypes)Enum.Parse(typeof(ValgTypes), ConfigurationManager.AppSettings["valgType"]);
                server = ConfigurationManager.AppSettings["queryServer"];
                interval = Convert.ToInt32(ConfigurationManager.AppSettings["queryInterval"]) * 1000;
                timeout = Convert.ToInt32(ConfigurationManager.AppSettings["queryTimeout"]) * 1000;
                mInterval = Convert.ToInt32(ConfigurationManager.AppSettings["maintenanceInterval"]) * 1000;
                waitOnStop = Convert.ToInt32(ConfigurationManager.AppSettings["maxWaitOnStop"]) * 1000;
                enableSimpleSync = Convert.ToBoolean(ConfigurationManager.AppSettings["enableSimpleSync"]);
                simpleSyncInterval = Convert.ToInt32(ConfigurationManager.AppSettings["simpleSyncInterval"]);
                enableAdvancedSync = Convert.ToBoolean(ConfigurationManager.AppSettings["enableAdvancedSync"]);
                maxAsync = Convert.ToInt32(ConfigurationManager.AppSettings["maxAsyncPeriod"]);
                lastnodeCheck = Convert.ToBoolean(ConfigurationManager.AppSettings["checkLastnode"]);

                try
                {
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["certificatePath"]))
                        certificate = X509Certificate.CreateFromCertFile(ConfigurationManager.AppSettings["certificatePath"]);
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("Error loading certificate {0}, running without. {1}", ConfigurationManager.AppSettings["certificatePath"],ex.Message);
                }

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["fileCharSet"]))
                    file_charset = (ConfigurationManager.AppSettings["fileCharSet"]);

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["serverCharSet"]))
                    server_charset = (ConfigurationManager.AppSettings["serverCharSet"]);


                logger.DebugFormat("queryLogFolder - {0}", queryLogFolder);
                logger.InfoFormat("xmlOutFolder - {0}", xmlOutFolder);
                logger.InfoFormat("valgType - {0}", valgType);
                logger.InfoFormat("queryServer - {0}", server);
                logger.InfoFormat("certificate - {0}", certificate != null ?
                    certificate.Subject + " / " + certificate.GetCertHashString() + " / " + certificate.Issuer + " / " + certificate.GetSerialNumberString() : "N/A");
                logger.DebugFormat("server CharSet - {0}", server_charset);
                logger.DebugFormat("file CharSet - {0}", file_charset);
                logger.DebugFormat("queryInterval - {0} seconds", interval / 1000);
                logger.DebugFormat("queryTimeout - {0} seconds", timeout / 1000);
                logger.DebugFormat("checkLastnode - {0}", lastnodeCheck);
                logger.DebugFormat("maxWaitOnStop - {0} seconds", waitOnStop / 1000);
                logger.DebugFormat("maintenanceInterval - {0} seconds", mInterval / 1000);
                logger.InfoFormat("enableSimpleSync - {0}", enableSimpleSync);
                logger.DebugFormat("simpleSyncInterval - {0} minutes", simpleSyncInterval);
                logger.InfoFormat("enableAdvancedSync - {0}", enableAdvancedSync);
                logger.DebugFormat("maxAsyncPeriod - {0} minutes", maxAsync);
            }
            catch (Exception ex)
            {
                logger.Error("Failed to load settings. Aborting.", ex);
                NDC.Pop();
                throw ex;
            }

            //Get the report index, løpenummer
            index = 0;
            try
            {
                //Load last report
                XMLDoc.Load(Path.Combine(queryLogFolder, "lastnode.xml"));

                //Find the list nodes
                XmlNodeList nodelist = XMLDoc.SelectNodes("/liste");

                //Loop trough, find 'LopeNr'
                foreach (XmlNode node in nodelist)
                {
                    int idx = Convert.ToInt32(node.SelectSingleNode("data[@navn='RapportLopeNr']").InnerText);
                    if (idx > index) index = idx;
                }

                logger.InfoFormat("Report index (LøpeNr) loaded from disk: " + index);

            }
            catch (Exception ex)
            {
                logger.Warn("Failed to load last report index. All content will be refetched - " + ex.Message);
            }

            NDC.Pop();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            _busyEvent.Set();

            pollTimer.Interval = interval;
            pollTimer.Start();

            maintenanceTimer.Interval = mInterval;
            maintenanceTimer.Start();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            maintenanceTimer.Stop();

            if (!pollTimer.Enabled)
                logger.Info("Waiting for current R06 sequence to complete.");

            if (!_busyEvent.WaitOne(waitOnStop, false))
                logger.Warn("Last R06 sequence did not complete properly. Valgdata-files may be inconsistent.");

            pollTimer.Stop();
        }


        /// <summary>
        /// Handles the Tick event of the pollTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void pollTimer_Elapsed(object sender, EventArgs e)
        {
            logger.DebugFormat("pollTimer_Elapsed hit. Current index: {0}", index);

            pollTimer.Stop();
            _busyEvent.WaitOne();

            try
            {
                //Check lastnode.xml
                if (!LastnodeExists())
                    logger.Warn("Report index log lastnode.xml disappeared. Resetting report index to zero.");

                //Perform R06 query at each timer tick
                NDC.Push("R06");
                PerformR06Query();
                NDC.Pop();
            }
            catch (Exception ex)
            {
                NDC.Pop();
                logger.Error("R06 update sequence failed.", ex);
            }

            _busyEvent.Set();
            pollTimer.Start();
        }

        /// <summary>
        /// Handles the Elapsed event of the maintenanceTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            logger.DebugFormat("maintenanceTimer_Elapsed hit.", index);

            maintenanceTimer.Stop();
            _busyEvent.WaitOne();
            NDC.Push("MAINTENANCE");

            //Simple sync
            try
            {
                NDC.Push("SIMPLESYNC");
                if (enableSimpleSync && lastSimpleSync.AddMinutes(simpleSyncInterval) < DateTime.Now)
                {
                    if (valgType == ValgTypes.FK)
                    {
                        SimpleReportSync_F();
                        SimpleReportSync_K();
                    }
                    else
                    {
                        SimpleReportSync_ST();
                    }
                    lastSimpleSync = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Simple sync sequence failed.", ex);
            }
            finally
            {
                NDC.Pop();
            }

            //Update sync errors
            try
            {
                NDC.Push("ADVANCEDSYNC");
                if (enableAdvancedSync && valgType == ValgTypes.ST) 
                    RecheckST04SyncItems();
            }
            catch (Exception ex)
            {
                logger.Error("Advanced sync sequence failed.", ex);
            }
            finally
            {
                NDC.Pop();
            }

            NDC.Pop();
            _busyEvent.Set();
            maintenanceTimer.Start();

        }

        /// <summary>
        /// Checks if the lastnode-xml is present while running. 
        /// </summary>
        private bool LastnodeExists()
        {
            if (index > 0 && lastnodeCheck && !File.Exists(Path.Combine(queryLogFolder, "lastnode.xml")))
            {
                index = 0;
                return false;
            }

            return true;
        }

        //Performing the timed R06 query
        /// <summary>
        /// Performs the R06 query.
        /// </summary>
        private void PerformR06Query()
        {

            //Vars
            string query;
            string R06Response;
            string response;
            string valg = string.Empty;
            XmlNodeList nodeList = default(XmlNodeList);
            XmlNode node = default(XmlNode);

            //Reset lists
            fetchedKommuner.Clear();
            fetchedFylker.Clear();
            fetchedBydeler.Clear();

            //Build R06 query
            query = ValgUtils.CreateRESTR06Query(index);

            //Fetch the XML data
            R06Response = ValgUtils.DoQuery(query);

            try
            {
                if (logger.IsDebugEnabled)
                    ValgUtils.DumpToFile(R06Response, Path.Combine(queryLogFolder, "R06-DEBUG"), "R06-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                //Get the processed XML
                XMLDoc = ValgUtils.GetXmlDocument(R06Response);

                //Find the list nodes
                nodeList = XMLDoc.SelectNodes("//rapport/tabell/liste");

                //By now we know this is proper XML
                if (nodeList.Count > 0)
                    logger.InfoFormat("R06 query result: {0} update entries", nodeList.Count);
                else
                    logger.DebugFormat("R06 query result: {0} update entries", nodeList.Count);
            }
            catch (ValgException valgEx)
            {
                logger.Warn("Valg XML report error: " + valgEx.Message);
                throw valgEx;
            }
            catch (XmlException xmlEx)
            {
                logger.Warn("Invalid R06 XML report, bad response: " + xmlEx.Message);
                throw xmlEx;
            }
            catch (IOException fileEx)
            {
                logger.Warn("Failed to save R06 content to disk: " + fileEx.Message);
                throw fileEx;
            }
            catch (Exception ex)
            {
                logger.Warn("General error processing R06 report" + ex.Message);
                throw ex;
            }

            //Dump to disk if we had updates, these are not necessearily all updated yet!
            if (nodeList.Count > 0)
            {
                //Current
                ValgUtils.DumpToFile(XMLDoc, queryLogFolder, "R06.xml");
                //Archive
                ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, "R06"), "R06-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");
            }

            //Refetch ST06 if dirty
            if (dirtyST06)
            {
                reportFetchFlags["ST06"] = "Landsoversikt";
                reportFetchFlags["ST07"] = "Stortingsprognose";
                reportFetchFlags["ST14"] = "Fremmøte, høy/lav";
            }

            //Loop trough, find 'LopeNr' and handle them individually
            //Updating kommune/fylke for each element individually
            int i = 0;
            for (i = nodeList.Count - 1; i >= 0; i -= 1)
            {
                node = nodeList[i];

                //LopeNr - Index
                int idx = Convert.ToInt32(node.SelectSingleNode("data[@navn='RapportLopeNr']").InnerText);
                valg = node.SelectSingleNode("data[@navn='Valgtype']").InnerText;
                string kretsNr = node.SelectSingleNode("data[@navn='KretsNr']").InnerText;
                string kommNr = node.SelectSingleNode("data[@navn='KommNr']").InnerText;

                if (valg == "ST")
                {
                    //Stortingsvalg common reports
                    dirtyST06 = false;
                    reportFetchFlags["ST06"] = "Landsoversikt";
                    reportFetchFlags["ST04"] = "Komplett fylkesoversikt";
                    reportFetchFlags["ST07"] = "Stortingsprognose";
                    reportFetchFlags["ST14"] = "Fremmøte, høy/lav";
                    if (!string.IsNullOrEmpty(kretsNr))
                        reportFetchFlags["ST03"] = "Komplett kretsoversikt";
                }
                else if (valg == "FY")
                {
                    //Fylkestingsvalg common reports
                    reportFetchFlags["F01"] = "Komplett opptellingsstatus";
                    reportFetchFlags["F04"] = "Komplett fylkesoversikt";
                    reportFetchFlags["F05"] = "Landsoversikt";
                    reportFetchFlags["F07"] = "Landsoversikt per fylke";
                    if (!string.IsNullOrEmpty(kretsNr))
                        reportFetchFlags["F03"] = "Komplett kretsoversikt";
                }
                else if (valg == "KO")
                {
                    //Kommunevalg common reports
                    reportFetchFlags["K01"] = "Komplett opptellingsstatus";
                    reportFetchFlags["K04"] = "Komplett fylkesoversikt";
                    reportFetchFlags["K05"] = "Landsoversikt";
                    reportFetchFlags["K07"] = "Fremmøte, høy/lav";
                    if (!string.IsNullOrEmpty(kretsNr))
                        reportFetchFlags["K03"] = "Komplett kretsoversikt";

                    //Oslo = 0301
                    if (kommNr == "0301")
                    {
                        reportFetchFlags["K09"] = "Bystyreoversikt";
                        reportFetchFlags["F01"] = "Komplett opptellingsstatus - Oslo";
                        reportFetchFlags["F03"] = "Komplett kretsoversikt - Oslo";
                        reportFetchFlags["F04"] = "Komplett fylkesoversikt - Oslo";
                    }
                }

                else if (valg == "SA")
                {
                    //Sametingsvalg, nothing to do yet
                }

                //Fetch other reports based on current entry
                try
                {
                    //Log context
                    NDC.Push(string.Format("IDX:{0}", idx));

                    //Actual update
                    if (valg == "SA")
                    {
                        //Do nothing yet
                        logger.Debug("Skipping update for Sametingsvalget");
                    }
                    else
                    {
                        HandleUpdateEntry(node);
                    }

                    //Update index either way
                    if (idx > index) index = idx;
                }
                finally
                {
                    NDC.Pop();
                }
            }

            //Report aggregation first
            string key = "";
            try
            {
                foreach (string k in Enum.GetNames(typeof(AggregateTypes)))
                {
                    key = k;
                    if (reportFetchFlags.ContainsKey(key))
                    {
                        valg = key.Substring(0, key.Length - 2);
                        XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), key));
                        logger.InfoFormat("Reports aggregated: {0} / {1}", key, reportFetchFlags[key]);
                        reportFetchFlags.Remove(key);

                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, key + ".xml");
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + key), key + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");
                    }
                }
            }
            catch (XmlException xmlEx)
            {
                logger.Warn("Invalid aggregation XML content: " + key + " / " + reportFetchFlags[key] + " - " + xmlEx.Message);
                throw xmlEx;
            }
            catch (IOException fileEx)
            {
                logger.Warn("Failed to save content to disk: " + key + " / " + reportFetchFlags[key] + " - " + fileEx.Message);
                throw fileEx;
            }
            catch (Exception ex)
            {
                logger.Warn("General error aggreating reports: " + key + " / " + reportFetchFlags[key] + " - " + ex.Message);
                throw ex;
            }

            //Then do all delayed reports from the fetchflags table
            foreach (string report in reportFetchFlags.Keys)
            {
                //Send query
                string r = report.ToUpper();
                query = ValgUtils.CreateRESTQuery(r, string.Empty, string.Empty);
                response = ValgUtils.DoQuery(query);

                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    valg = r.Substring(0, r.Length - 2);
                    logger.InfoFormat("Report fetched: {0} / {1}", r, reportFetchFlags[report]);
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, r + ".xml");
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + r), r + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                    ValgUtils.UpdateURLCache(query, r + ".xml");

                    //Reset dirty flags
                    if (r == "ST06") dirtyST06 = false;
                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + r + " / " + reportFetchFlags[report] + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + r + " / " + reportFetchFlags[report] + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + r + " / " + reportFetchFlags[report] + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + r + " / " + reportFetchFlags[report] + " - " + ex.Message);
                    throw ex;
                }

            }

            //At this point, all pending reports have been downloaded
            reportFetchFlags.Clear();

        }

        // Takes a list node from the R06 report and fetches/updates other reports based on the new info
        /// <summary>
        /// Handles the update entry.
        /// </summary>
        /// <param name="node">The node.</param>
        private void HandleUpdateEntry(XmlNode node)
        {
            //Extract some values from the R06 node
            string idx = node.SelectSingleNode("data[@navn='RapportLopeNr']").InnerText;
            string valg = node.SelectSingleNode("data[@navn='Valgtype']").InnerText;
            string fylkeNr = node.SelectSingleNode("data[@navn='FylkeNr']").InnerText;
            string kommNr = node.SelectSingleNode("data[@navn='KommNr']").InnerText;
            string kretsNr = node.SelectSingleNode("data[@navn='KretsNr']").InnerText;
            string kommNavn = node.SelectSingleNode("data[@navn='KommNavn']").InnerText;
            string kretsNavn = node.SelectSingleNode("data[@navn='KretsNavn']").InnerText;
            string status = node.SelectSingleNode("data[@navn='StatusInd']").InnerText;

            //Response extractors
            XmlNode n;
            string rspNavn;
            string rspOmrNr;
            string rspKretsNr;
            string rspStatus;

            //Misc. other placeholders
            string tmp;
            string report;
            string query;
            string response;

            //Accont for differences
            valg = ValgUtils.GetReportPrefix(valg);


            //Applies to stortingsvalg only
            if (valgType == ValgTypes.ST)
            {
                //Update sync items for the XX04 report
                SyncItem s = new SyncItem();
                s.valg = valg;
                s.report = valg + "04";
                s.fylkeNr = fylkeNr;
                s.kommuneNr = kommNr;
                s.kretsNr = kretsNr;
                s.kommune = kommNavn;
                s.krets = kretsNavn;
                s.expectedStatus = status;
                s.timestamp = DateTime.Now;

                //TODO: Expand sync tables for new elections

                //Maintain mapping objects
                if (!syncItems_ST04.ContainsKey(fylkeNr))
                {
                    syncItems_ST04[fylkeNr] = new Dictionary<string, Dictionary<string, SyncItem>>();
                    syncItems_ST04[fylkeNr][kommNr] = new Dictionary<string, SyncItem>();
                }
                else if (!syncItems_ST04[fylkeNr].ContainsKey(kommNr))
                {
                    syncItems_ST04[fylkeNr][kommNr] = new Dictionary<string, SyncItem>();
                }

                //Update
                syncItems_ST04[fylkeNr][kommNr][kretsNr] = s;
            }

            //Fetch the X03 report (Enkeltoversikt pr krets) if applicable
            if (!string.IsNullOrEmpty(kretsNr))
            {
                //Create and do query
                report = valg + "03";
                query = ValgUtils.CreateRESTQuery(report, kommNr, kretsNr);
                response = ValgUtils.DoQuery(query);

                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNavn']");
                    rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNr']");
                    rspOmrNr = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KretsNr']");
                    rspKretsNr = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='StatusInd']");
                    rspStatus = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Krets: {1}/{2} ({3} - {4}) Status: {5}", report, rspOmrNr, kretsNr, rspNavn, kretsNavn, rspStatus);

                    // Extra sanity check
                    if (status != rspStatus)
                        logger.WarnFormat("Report error: {0}, StatusInd mismatch: R06-value: {1}, {2}-value: {3}", report, status, report, rspStatus);

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}-{3}.xml", report, rspOmrNr, rspKretsNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));

                    //If this is Oslo (K), we need to manually generate an F03 as a copy of K03
                    if (valgType == ValgTypes.FK && kommNr == "0301")
                    {
                        //F03 Create and do query
                        String old = report;
                        report = "F03";

                        //Update report number
                        n = XMLDoc.SelectSingleNode("//rapport/rapportnavn");
                        n.InnerText = report;
                        XmlNodeList nl = XMLDoc.SelectNodes("//rapport/tabell");
                        foreach (XmlNode item in nl)
                            item.Attributes["navn"].Value = item.Attributes["navn"].Value.Replace(old, report);


                        //Extract some values
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNavn']");
                        rspNavn = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNr']");
                        rspOmrNr = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KretsNr']");
                        rspKretsNr = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='StatusInd']");
                        rspStatus = n.InnerText;

                        logger.InfoFormat("Report COPY generated: {0}, Krets: {1}/{2} ({3} - {4}) Status: {5}", report, rspOmrNr, kretsNr, rspNavn, kretsNavn, rspStatus);

                        // Extra sanity check
                        if (status != rspStatus)
                        {
                            //Log event
                            logger.WarnFormat("Report COPY error: {0}, StatusInd mismatch: R06-value: {1}, {2}-value (Kommune: {3}/{4}{5}): {6}",
                                report, status, report, kommNr, kommNavn,
                                (string.IsNullOrEmpty(kretsNr) ? "" : "/" + kretsNavn), (string.IsNullOrEmpty(rspStatus) ? "<blank>" : rspStatus));
                        }

                        //Save to disk
                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, @"F\" + report),
                            string.Format("{0}-{1}-{2}-{3}.xml", report, rspOmrNr, rspKretsNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                        ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));

                        //To temp if this is part of an agregated report
                        if (Enum.IsDefined(typeof(AggregateTypes), report))
                            ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));
                    }
                
                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.WarnFormat("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }

            }

            //Fetch the Bydelsoversikt, Oslo if applicable.
            //Report IDs may vary, using helpers to determine
            if (kommNr == "0301" && !String.IsNullOrEmpty(ValgUtils.GetBydelsReport(valg)))
            {
                string bdnr = ValgUtils.GetBydelsNumber(kretsNr);
                tmp = String.Format("{0}{1}", valg, bdnr   );
                int t = 0;
                if (Int32.TryParse(bdnr, out t) && !fetchedBydeler.Contains(tmp))
                {
                    //Get report data
                    report = ValgUtils.GetBydelsReport(valg);
                    query = ValgUtils.CreateRESTQuery(report, bdnr, string.Empty);
                    response = ValgUtils.DoQuery(query);

                    //Because of the varying report names, we have to update the aggregate report fetch flags here
                    reportFetchFlags[report] = "Komplett bydelseoversikt, Oslo";

                    //Try to load the XML and extract som key data
                    try
                    {
                        //Get the processed XML
                        XMLDoc = ValgUtils.GetXmlDocument(response);

                        //Extract some values
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='BydelNavn']");
                        rspNavn = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='BydelNr']");
                        rspOmrNr = n.InnerText;

                        logger.InfoFormat("Report fetched: {0}, Oslo, Bydel: {1}/{2}", report, rspOmrNr, rspNavn);

                        //Save to disk
                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                            string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                        ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));

                        //To temp if this is part of an agregated report
                        if (Enum.IsDefined(typeof(AggregateTypes), report))
                            ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                        //Update bydel fetchlog
                        fetchedBydeler.Add(tmp);
                    }
                    catch (ValgException valgEx)
                    {
                        logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                        throw valgEx;
                    }
                    catch (XmlException xmlEx)
                    {
                        logger.WarnFormat("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                        throw xmlEx;
                    }
                    catch (IOException fileEx)
                    {
                        logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                        throw fileEx;
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("General error processing report: " + report + " - " + ex.Message);
                        throw ex;
                    }

                }
            }

            //Fetch the X02 report (kommuneoversikt) if applicable
            tmp = String.Format("{0}{1}", valg, kommNr);
            if (!fetchedKommuner.Contains(tmp))
            {
                //Create and do query
                report = valg + "02";
                query = ValgUtils.CreateRESTQuery(report, kommNr, string.Empty);
                response = ValgUtils.DoQuery(query);

                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNavn']");
                    rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNr']");
                    rspOmrNr = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='StatusInd']");
                    rspStatus = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Kommune: {1} ({2}) Status: {3}",
                        report, rspOmrNr, rspNavn, (string.IsNullOrEmpty(kretsNr) ? rspStatus : "N/A"));

                    // Extra sanity check
                    if (status != rspStatus && string.IsNullOrEmpty(kretsNr))
                        logger.WarnFormat("Report error: {0}, StatusInd mismatch: R06-value: {1}, {2}-value: {3}", report, status, report, rspStatus);

                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                    //Update fylke fetchlog
                    fetchedKommuner.Add(tmp);
                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }
            else
                logger.InfoFormat("Report already fetched this round: {0}, Kommune: {1} ({2}) Status: {3}", valg + "02", kommNr, kommNavn, string.IsNullOrEmpty(kretsNr) ? status : "N/A");


            //Fetch the X04 report (Fylkesoversikt) if applicable
            tmp = String.Format("{0}{1}", valg, fylkeNr);
            if (!fetchedFylker.Contains(tmp))
            {
                //Create and do query
                report = valg + "04";
                query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                response = ValgUtils.DoQuery(query);

                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    rspOmrNr = n.InnerText;
                    logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                    //Status check is only available for valgtype ST
                    if (valgType == ValgTypes.ST)
                    {
                        if (!string.IsNullOrEmpty(kretsNr))
                        {
                            n = XMLDoc.SelectSingleNode("//rapport/tabell[@navn='" + report + "tabell3']/liste[data[@navn = 'KretsNr']='" + kretsNr + "']/data[@navn = 'StatusInd']");
                            rspStatus = n.InnerText;
                        }
                        else
                        {
                            n = XMLDoc.SelectSingleNode("//rapport/tabell[@navn='" + report + "tabell3']/liste[data[@navn = 'KommNr']='" + kommNr + "']/data[@navn = 'StatusInd']");
                            rspStatus = n.InnerText;
                        }

                        // Extra sanity check
                        if (status != rspStatus)
                        {
                            //Log event
                            logger.WarnFormat("Report error: {0}, StatusInd mismatch: R06-value: {1}, {2}-value (Kommune: {3}/{4}{5}): {6}",
                                report, status, report, kommNr, kommNavn,
                                (string.IsNullOrEmpty(kretsNr) ? "" : "/" + kretsNavn), (string.IsNullOrEmpty(rspStatus) ? "<blank>" : rspStatus));
                        }
                    }

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));


                    //Fetch the X01 report (Opptellingsstatus) if applicable
                    if (valgType == ValgTypes.FK)
                    {
                        //Create and do query
                        report = valg + "01";
                        query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                        response = ValgUtils.DoQuery(query);

                        //Get the processed XML
                        XMLDoc = ValgUtils.GetXmlDocument(response);

                        //Extract some values
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                        rspNavn = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                        rspOmrNr = n.InnerText;
                        
                        logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                        // NO Extra status sanity check here - needed data is missing from F/K01

                        //Save to disk
                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                            string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                        ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));

                        //To temp if this is part of an agregated report
                        if (Enum.IsDefined(typeof(AggregateTypes), report))
                            ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    }

                    //If this is Oslo (K), we need to manually fetch F01/F04 also
                    if (valgType == ValgTypes.FK && kommNr == "0301")
                    {
                        //F01 Create and do query
                        report = "F01";
                        query = ValgUtils.CreateRESTQuery(report, "03", string.Empty);
                        response = ValgUtils.DoQuery(query);

                        //Get the processed XML
                        XMLDoc = ValgUtils.GetXmlDocument(response);

                        //Extract some values
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                        rspNavn = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                        rspOmrNr = n.InnerText;

                        logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                        //NO Extra status sanity check here - needed data is missing from F/K01

                        //Save to disk
                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, @"F\" + report),
                            string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                        ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        
                        //To temp if this is part of an agregated report
                        if (Enum.IsDefined(typeof(AggregateTypes), report))
                            ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));


                        //F04 Create and do query
                        report = "F04";
                        query = ValgUtils.CreateRESTQuery(report, "03", string.Empty);
                        response = ValgUtils.DoQuery(query);

                        //Get the processed XML
                        XMLDoc = ValgUtils.GetXmlDocument(response);

                        //Extract some values
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                        rspNavn = n.InnerText;
                        n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                        rspOmrNr = n.InnerText;
                        logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                        //Save to disk
                        ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, @"F\" + report),
                            string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                        ValgUtils.UpdateURLCache(query, string.Format("{0}-{1}.xml", report, rspOmrNr));

                        //To temp if this is part of an agregated report
                        if (Enum.IsDefined(typeof(AggregateTypes), report))
                            ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    }

                    //Update fylke fetchlog
                    fetchedFylker.Add(tmp);

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }

            //Update last-node log if we're ok
            try
            {
                if (LastnodeExists())
                {
                    XmlTextWriter w = new XmlTextWriter(Path.Combine(queryLogFolder, "lastnode.xml"), Encoding.GetEncoding(file_charset));
                    w.Formatting = Formatting.Indented;
                    w.Indentation = 2;
                    w.IndentChar = ' ';
                    w.WriteStartDocument(true);
                    node.WriteTo(w);
                    w.Close();
                }
                else
                    throw new ValgException("Report index log lastnode.xml disappeared while updating individual R06 items. Resetting report index.");

            }
            catch (ValgException valgEx)
            {
                logger.Warn("Valg: " + valg + " - " + valgEx.Message);
                throw valgEx;
            }
            catch (XmlException xmlEx)
            {
                logger.Warn("Invalid XML, failed to handle lastnode content: " + valg + " - " + xmlEx.Message);
                logger.Debug(node.OuterXml);
                throw xmlEx;
            }
            catch (IOException fileEx)
            {
                logger.Warn("Failed to save lastnode content to disk: " + valg + " - " + fileEx.Message);
                throw fileEx;
            }
            catch (Exception ex)
            {
                logger.Warn("General error processing lastnode content: " + valg + " - " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Aggregates reports on disk to one large chunk. Supports X03 (kretser) and X04 (fylker)
        /// </summary>
        private XmlDocument AggregateReports(AggregateTypes t)
        {
            //Files to aggregate
            string[] files;

            //Check which report to aggregate
            switch (t)
            {
                case AggregateTypes.ST03:
                    files = Directory.GetFiles(tempFolder, "ST03-*.xml");
                    break;
                case AggregateTypes.ST04:
                    files = Directory.GetFiles(tempFolder, "ST04-*.xml");
                    break;
                case AggregateTypes.ST05:
                    files = Directory.GetFiles(tempFolder, "ST05-*.xml");
                    break;
                case AggregateTypes.F01:
                    files = Directory.GetFiles(tempFolder, "F01-*.xml");
                    break;
                case AggregateTypes.F03:
                    files = Directory.GetFiles(tempFolder, "F03-*.xml");
                    break;
                case AggregateTypes.F04:
                    files = Directory.GetFiles(tempFolder, "F04-*.xml");
                    break;
                case AggregateTypes.K01:
                    files = Directory.GetFiles(tempFolder, "K01-*.xml");
                    break;
                case AggregateTypes.K03:
                    files = Directory.GetFiles(tempFolder, "K03-*.xml");
                    break;
                case AggregateTypes.K04:
                    files = Directory.GetFiles(tempFolder, "K04-*.xml");
                    break;
                case AggregateTypes.K08:
                    files = Directory.GetFiles(tempFolder, "K08-*.xml");
                    break;
                default:
                    throw new NotSupportedException("Unsupported report aggregate type");
            }

            logger.InfoFormat("Aggregating {0} reports: {1} pieces", Enum.GetName(typeof(AggregateTypes), t), files.GetLength(0));

            //Aggregate doc
            XmlDocument ret = ValgUtils.GetEmptyResponseXML();

            //Temp 
            XmlDocument tmpDoc = new XmlDocument();
            XmlNode tmpNode;

            //Loop over reports, aggregate and save
            foreach (string s in files)
            {
                tmpDoc.Load(s);
                tmpNode = ret.ImportNode(tmpDoc.DocumentElement.FirstChild, true);
                ret.DocumentElement.AppendChild(tmpNode);

                if (logger.IsDebugEnabled)
                {
                    string t1, t2, t3, t4, t5;
                    t1 = tmpDoc.SelectSingleNode("//rapport/rapportnavn").InnerText;

                    t2 = tmpDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr' or @navn='KommNr' or @navn='BydelNr']").InnerText;
                    t3 = tmpDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn' or @navn='KommNavn']").InnerText;

                    //Kretser
                    if (t == AggregateTypes.ST03 || t == AggregateTypes.F03 || t == AggregateTypes.K03)
                    {
                        t4 = tmpDoc.SelectSingleNode("//rapport/data[@navn='KretsNr']").InnerText;
                        t5 = tmpDoc.SelectSingleNode("//rapport/data[@navn='KretsNavn']").InnerText;
                        logger.DebugFormat("Aggregated report {0} - {1} ({2}) / {3} ({4}) ", t1, t2, t3, t4, t5);
                    }

                    //Bydeler
                    else if (t == AggregateTypes.ST05 || t == AggregateTypes.K08)
                    {
                        t4 = tmpDoc.SelectSingleNode("//rapport/data[@navn='BydelNavn']").InnerText;
                        logger.DebugFormat("Aggregated report {0} - {1}/{2} ({3}) ", t1, t2, t3, t4);
                    }

                    //Annet
                    else
                    {
                        logger.DebugFormat("Aggregated report {0} - {1} ({2})", t1, t2, t3);
                    }
                }
            }

            //Return aggregated doc
            return ret;
        }

        /// <summary>
        /// Rechecks the ST04 sync items.
        /// </summary>
        private void RecheckST04SyncItems()
        {
            String valg = "ST";
            String report = valg + "04";
            fetchedFylker.Clear();

            try
            {
                String[] reports = Directory.GetFiles(tempFolder, valg + "04-*.xml");
                foreach (String file in reports)
                {
                    XmlDocument xmlReport = new XmlDocument();
                    List<String> kommToRemove = new List<String>();

                    //Load XML
                    xmlReport.Load(file);

                    //Extract some values
                    XmlNode n = xmlReport.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    String fylkeNr = n.InnerText;
                    n = xmlReport.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    String fylkeNavn = n.InnerText;

                    //Initial check
                    if (!syncItems_ST04.ContainsKey(fylkeNr)) continue;

                    //Loop over kommuner
                    foreach (KeyValuePair<String, Dictionary<String, SyncItem>> kvp in syncItems_ST04[fylkeNr])
                    {
                        List<String> kretsToRemove = new List<String>();
                        foreach (KeyValuePair<String, SyncItem> kvpi in kvp.Value)
                        {
                            //Kommuner
                            if (String.IsNullOrEmpty(kvpi.Key))
                            {
                                n = xmlReport.SelectSingleNode("//rapport/tabell[@navn='" + report + "tabell3']/liste[data[@navn = 'KommNr']='" + kvp.Key + "']/data[@navn = 'StatusInd']");
                                if (kvp.Value[String.Empty].expectedStatus == n.InnerText)
                                    kommToRemove.Add(kvp.Key);
                                else
                                {
                                    logger.InfoFormat("ST04 KOMMUNE-item is out of sync: {0}/{1}-{2}/{3} Expected: {4} Actuall: {5} Since: {6}",
                                        kvp.Value[String.Empty].fylkeNr, fylkeNavn, kvp.Value[String.Empty].kommuneNr, kvp.Value[String.Empty].kommune,
                                        kvp.Value[String.Empty].expectedStatus, (string.IsNullOrEmpty(n.InnerText) ? "<blank>" : n.InnerText), kvp.Value[String.Empty].timestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                                    //Update fylke fetchlog
                                    if ((DateTime.Now - kvp.Value[String.Empty].timestamp).TotalMinutes >= maxAsync && !fetchedFylker.Contains(fylkeNr))
                                        fetchedFylker.Add(fylkeNr);
                                }
                            }
                            //Kretser
                            else
                            {
                                n = xmlReport.SelectSingleNode("//rapport/tabell[@navn='" + report + "tabell3']/liste[data[@navn = 'KretsNr']='" + kvpi.Key + "']/data[@navn = 'StatusInd']");
                                if (kvpi.Value.expectedStatus == n.InnerText)
                                    kretsToRemove.Add(kvpi.Key);
                                else if (overrideStatusValues[kvpi.Value.kommuneNr].ContainsKey(kvpi.Key) && n.InnerText == "8" &&
                                    overrideStatusValues[kvpi.Value.kommuneNr][kvpi.Key] == kvpi.Value.expectedStatus)
                                {
                                    logger.InfoFormat("ST04 KRETS-item   status sync overridden: {0}/{1}-{2}/{3}-{4}/{5} Expected: {6} Max override: {7} Actuall: {8} Since: {9}",
                                        kvpi.Value.fylkeNr, fylkeNavn, kvpi.Value.kommuneNr, kvpi.Value.kommune, kvpi.Value.kretsNr, kvpi.Value.krets,
                                        kvpi.Value.expectedStatus, overrideStatusValues[kvpi.Value.kommuneNr][kvpi.Key], (string.IsNullOrEmpty(n.InnerText) ? "<blank>" : n.InnerText),
                                        kvpi.Value.timestamp.ToString("yyyy-MM-dd HH:mm:ss"));
                                    kretsToRemove.Add(kvpi.Key);
                                }
                                else
                                {
                                    logger.InfoFormat("ST04 KRETS-item   is out of sync: {0}/{1}-{2}/{3}-{4}/{5} Expected: {6} Actuall: {7} Since: {8}",
                                        kvpi.Value.fylkeNr, fylkeNavn, kvpi.Value.kommuneNr, kvpi.Value.kommune, kvpi.Value.kretsNr, kvpi.Value.krets,
                                        kvpi.Value.expectedStatus, (string.IsNullOrEmpty(n.InnerText) ? "<blank>" : n.InnerText), kvpi.Value.timestamp.ToString("yyyy-MM-dd HH:mm:ss"));

                                    //Update fylke fetchlog
                                    if ((DateTime.Now - kvpi.Value.timestamp).TotalMinutes >= maxAsync && !fetchedFylker.Contains(fylkeNr))
                                        fetchedFylker.Add(fylkeNr);
                                }
                            }
                        }

                        //Actuall delete for kretser
                        foreach (String s in kretsToRemove)
                            kvp.Value.Remove(s);
                    }

                    //Actuall delete for kommuner
                    foreach (String s in kommToRemove)
                        syncItems_ST04[fylkeNr].Remove(s);
                }
            }
            catch (Exception ex)
            {
                logger.WarnFormat("Failed to update the {0} sync error list: {1}", report, ex.Message);
            }

            //Create and do queries
            foreach (String fylkeNr in fetchedFylker)
            {
                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    String query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                    String response = ValgUtils.DoQuery(query);
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    String rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    String rspOmrNr = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }

            //Add the latest fetched to the aggregate.
            if (fetchedFylker.Count > 0)
            {
                XmlDocument XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
                ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
                ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                logger.Debug("ST06/07 is now dirty. Reports will be refetched in the next loop.");
                dirtyST06 = true;
            }
        }

        /// <summary>
        /// Fetches ST04/06/07 for sync purpouses
        /// </summary>
        private void SimpleReportSync_ST()
        {
            String valg = "ST";
            String report = "ST05";
            String query;
            String response;

            // Kretser
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["refetchST03"]))
            {
                //All bydeler
                for (int i = 0; i < 16; i++)
                {
                        //Try to load the XML and extract som key data
                        try
                        {
                            //Get the processed XML
                            query = ValgUtils.CreateRESTQuery(report, i.ToString("00"), null);
                            response = ValgUtils.DoQuery(query);
                            XMLDoc = ValgUtils.GetXmlDocument(response);

                            //Extract some values
                            XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='BydelNavn']");
                            String rspNavn = n.InnerText;
                            n = XMLDoc.SelectSingleNode("//rapport/data[@navn='BydelNr']");
                            String rspOmrNr = n.InnerText;

                            logger.InfoFormat("Report fetched: {0}, Oslo, Bydel: {1}/{2}", report, rspOmrNr, rspNavn);

                            //Save to disk
                            ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                            ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                                string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                            //To temp if this is part of an agregated report
                            if (Enum.IsDefined(typeof(AggregateTypes), report))
                                ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                        }
                        catch (ValgException valgEx)
                        {
                            logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                            throw valgEx;
                        }
                        catch (XmlException xmlEx)
                        {
                            logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                            throw xmlEx;
                        }
                        catch (IOException fileEx)
                        {
                            logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                            throw fileEx;
                        }
                        catch (Exception ex)
                        {
                            logger.Warn("General error processing report: " + report + " - " + ex.Message);
                            throw ex;
                        }
                }

                //Agregate the ST05
                XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
                ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
                ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                valg = "ST";
                report = "ST03";

                //Get all kretser - stupid SSB
                foreach (string ko in overrideStatusValues.Keys)
                {
                    foreach (string kr in overrideStatusValues[ko].Keys)
                    {
                        //Try to load the XML and extract som key data
                        try
                        {
                            //Get the processed XML
                            query = ValgUtils.CreateRESTQuery(report, ko, kr);
                            response = ValgUtils.DoQuery(query);
                            XMLDoc = ValgUtils.GetXmlDocument(response);

                            //Extract some values
                            XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNavn']");
                            String rspNavn = n.InnerText;
                            n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KommNr']");
                            String rspOmrNr = n.InnerText;
                            n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KretsNr']");
                            String rspKretsNr = n.InnerText;
                            n = XMLDoc.SelectSingleNode("//rapport/data[@navn='KretsNavn']");
                            String rspKretsNavn = n.InnerText;

                            logger.InfoFormat("Report fetched: {0}, Krets: {1}/{2} ({3} - {4})", report, rspOmrNr, rspKretsNr, rspNavn, rspKretsNavn);

                            //Save to disk
                            ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));
                            ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                                string.Format("{0}-{1}-{2}-{3}.xml", report, rspOmrNr, rspKretsNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                            //To temp if this is part of an agregated report
                            if (Enum.IsDefined(typeof(AggregateTypes), report))
                                ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, rspKretsNr));
                        }
                        catch (ValgException valgEx)
                        {
                            logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                            throw valgEx;
                        }
                        catch (XmlException xmlEx)
                        {
                            logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                            throw xmlEx;
                        }
                        catch (IOException fileEx)
                        {
                            logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                            throw fileEx;
                        }
                        catch (Exception ex)
                        {
                            logger.Warn("General error processing report: " + report + " - " + ex.Message);
                            throw ex;
                        }
                    }
                }

                //Agregate the ST03
                XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
                ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
                ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");
            }

            //Fylker
            valg = "ST";
            report = "ST04";

            fetchedFylker.Clear();
            fetchedFylker.Add("01");
            fetchedFylker.Add("02");
            fetchedFylker.Add("03");
            fetchedFylker.Add("04");
            fetchedFylker.Add("05");
            fetchedFylker.Add("06");
            fetchedFylker.Add("07");
            fetchedFylker.Add("08");
            fetchedFylker.Add("09");
            fetchedFylker.Add("10");
            fetchedFylker.Add("11");
            fetchedFylker.Add("12");
            fetchedFylker.Add("14");
            fetchedFylker.Add("15");
            fetchedFylker.Add("16");
            fetchedFylker.Add("17");
            fetchedFylker.Add("18");
            fetchedFylker.Add("19");
            fetchedFylker.Add("20");

            //Create and do queries
            foreach (String fylkeNr in fetchedFylker)
            {
                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                    response = ValgUtils.DoQuery(query);
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    String rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    String rspOmrNr = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }

            //Agregate the ST04
            XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
            ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
            ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

            //Global reports
            reportFetchFlags.Clear();
            reportFetchFlags["ST06"] = "Landsoversikt";
            reportFetchFlags["ST07"] = "Stortingsprognose";
            reportFetchFlags["ST14"] = "Fremmøte, høy/lav";
            foreach (String rep in reportFetchFlags.Keys)
            {
                //Send query
                string r = rep.ToUpper();
                query = ValgUtils.CreateRESTQuery(r, string.Empty, string.Empty);
                response = ValgUtils.DoQuery(query);

                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    valg = r.Substring(0, r.Length - 2);
                    logger.InfoFormat("Report fetched: {0} / {1}", r, reportFetchFlags[rep]);
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, r + ".xml");
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + r), r + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + r + " / " + reportFetchFlags[rep] + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + r + " / " + reportFetchFlags[rep] + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + r + " / " + reportFetchFlags[rep] + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + r + " / " + reportFetchFlags[rep] + " - " + ex.Message);
                    throw ex;
                }

            }
        }

        /// <summary>
        /// Fetches F04/05/07 for sync purpouses
        /// </summary>
        private void SimpleReportSync_F()
        {
            String valg = "F";
            String report = "F04";
            String query;
            String response;

            fetchedFylker.Clear();
            fetchedFylker.Add("01");
            fetchedFylker.Add("02");
            fetchedFylker.Add("03");
            fetchedFylker.Add("04");
            fetchedFylker.Add("05");
            fetchedFylker.Add("06");
            fetchedFylker.Add("07");
            fetchedFylker.Add("08");
            fetchedFylker.Add("09");
            fetchedFylker.Add("10");
            fetchedFylker.Add("11");
            fetchedFylker.Add("12");
            fetchedFylker.Add("14");
            fetchedFylker.Add("15");
            fetchedFylker.Add("16");
            fetchedFylker.Add("17");
            fetchedFylker.Add("18");
            fetchedFylker.Add("19");
            fetchedFylker.Add("20");

            //Create and do queries
            foreach (String fylkeNr in fetchedFylker)
            {
                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                    response = ValgUtils.DoQuery(query);
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    String rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    String rspOmrNr = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }

            //Agregate the F04
            XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
            ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
            ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

            //Global reports
            reportFetchFlags.Clear();
            reportFetchFlags["F05"] = "Landsoversikt";
            reportFetchFlags["F07"] = "Landsoversikt per fylke";
            foreach (String rep in reportFetchFlags.Keys)
            {
                //Send query
                string r = rep.ToUpper();
                query = ValgUtils.CreateRESTQuery(r, string.Empty, string.Empty);
                response = ValgUtils.DoQuery(query);

                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    valg = r.Substring(0, r.Length - 2);
                    logger.InfoFormat("Report fetched: {0} / {1}", r, reportFetchFlags[rep]);
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, r + ".xml");
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + r), r + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + r + " / " + reportFetchFlags[rep] + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + r + " / " + reportFetchFlags[rep] + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + r + " / " + reportFetchFlags[rep] + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + r + " / " + reportFetchFlags[rep] + " - " + ex.Message);
                    throw ex;
                }

            }
        }

        /// <summary>
        /// Fetches K04/05 for sync purpouses
        /// </summary>
        private void SimpleReportSync_K()
        {
            String valg = "K";
            String report = "K04";
            String query;
            String response;

            fetchedFylker.Clear();
            fetchedFylker.Add("01");
            fetchedFylker.Add("02");
            fetchedFylker.Add("03");
            fetchedFylker.Add("04");
            fetchedFylker.Add("05");
            fetchedFylker.Add("06");
            fetchedFylker.Add("07");
            fetchedFylker.Add("08");
            fetchedFylker.Add("09");
            fetchedFylker.Add("10");
            fetchedFylker.Add("11");
            fetchedFylker.Add("12");
            fetchedFylker.Add("14");
            fetchedFylker.Add("15");
            fetchedFylker.Add("16");
            fetchedFylker.Add("17");
            fetchedFylker.Add("18");
            fetchedFylker.Add("19");
            fetchedFylker.Add("20");

            //Create and do queries
            foreach (String fylkeNr in fetchedFylker)
            {
                //Try to load the XML and extract som key data
                try
                {
                    //Get the processed XML
                    query = ValgUtils.CreateRESTQuery(report, fylkeNr, string.Empty);
                    response = ValgUtils.DoQuery(query);
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    //Extract some values
                    XmlNode n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNavn']");
                    String rspNavn = n.InnerText;
                    n = XMLDoc.SelectSingleNode("//rapport/data[@navn='FylkeNr']");
                    String rspOmrNr = n.InnerText;

                    logger.InfoFormat("Report fetched: {0}, Fylke: {1} ({2})", report, rspOmrNr, rspNavn);

                    //Save to disk
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report),
                        string.Format("{0}-{1}-{2}.xml", report, rspOmrNr, DateTime.Now.ToString("yyyyMMdd_HHmmss.ff")));

                    //To temp if this is part of an agregated report
                    if (Enum.IsDefined(typeof(AggregateTypes), report))
                        ValgUtils.DumpToFile(XMLDoc, tempFolder, string.Format("{0}-{1}.xml", report, rspOmrNr));

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + report + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + report + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + report + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + report + " - " + ex.Message);
                    throw ex;
                }
            }

            //Agregate the K04
            XMLDoc = AggregateReports((AggregateTypes)Enum.Parse(typeof(AggregateTypes), report));
            ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, report + ".xml");
            ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + report), report + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

            //Global reports
            reportFetchFlags.Clear();
            reportFetchFlags["K05"] = "Landsoversikt";
            reportFetchFlags["K07"] = "Fremmøte, høy/lav";
            foreach (String rep in reportFetchFlags.Keys)
            {
                //Send query
                string r = rep.ToUpper();
                query = ValgUtils.CreateRESTQuery(r, string.Empty, string.Empty);
                response = ValgUtils.DoQuery(query);

                try
                {
                    //Get the processed XML
                    XMLDoc = ValgUtils.GetXmlDocument(response);

                    valg = r.Substring(0, r.Length - 2);
                    logger.InfoFormat("Report fetched: {0} / {1}", r, reportFetchFlags[rep]);
                    ValgUtils.DumpToFile(XMLDoc, xmlOutFolder, r + ".xml");
                    ValgUtils.DumpToFile(XMLDoc, Path.Combine(queryLogFolder, valg + @"\" + r), r + "-" + DateTime.Now.ToString("yyyyMMdd_HHmmss.ff") + ".xml");

                }
                catch (ValgException valgEx)
                {
                    logger.Warn("Valg XML report error: " + r + " / " + reportFetchFlags[rep] + " - " + valgEx.Message);
                    throw valgEx;
                }
                catch (XmlException xmlEx)
                {
                    logger.Warn("Invalid XML report, bad response: " + r + " / " + reportFetchFlags[rep] + " - " + xmlEx.Message);
                    throw xmlEx;
                }
                catch (IOException fileEx)
                {
                    logger.Warn("Failed to save content to disk: " + r + " / " + reportFetchFlags[rep] + " - " + fileEx.Message);
                    throw fileEx;
                }
                catch (Exception ex)
                {
                    logger.Warn("General error processing report: " + r + " / " + reportFetchFlags[rep] + " - " + ex.Message);
                    throw ex;
                }

            }
        }

    }

}
