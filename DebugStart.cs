﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ValgService2009
{
    /// <summary>
    /// Entry point when compiling for debugging as a WinForm
    /// </summary>
    static class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebugForm());

        }
    }
}
