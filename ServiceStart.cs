﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace ValgService2009
{
    /// <summary>
    /// Entry point when compiling to a full fledged windows service
    /// </summary>
    static class ServiceStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new QueryService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
