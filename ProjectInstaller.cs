﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

//Disable missing docs warning
#pragma warning disable 1591

namespace ValgService2009
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}

//Restore missing docs warnings
#pragma warning restore 1591

