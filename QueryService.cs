﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using log4net;

namespace ValgService2009
{
    /// <summary>
    /// Win32 Service class, used when running as a service
    /// </summary>
    public partial class QueryService : ServiceBase
    {
        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(QueryService));

        /// <summary>
        /// The actual main work object
        /// </summary>
        protected QueryComponent queryObject = new QueryComponent();

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryService"/> class.
        /// </summary>
        public QueryService()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                logger.Info("ValgService2009 starting");
                queryObject.Init();
                queryObject.Start();
                logger.Info("ValgService2009 started");
            }
            catch (Exception ex)
            {
                logger.Error("ValgService2009 DID NOT START properly", ex);
                queryObject.Stop();
            }

        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            queryObject.Stop();
            logger.Info("ValgService2009 stopped");
        }

        /// <summary>
        /// When implemented in a derived class, executes when the system is shutting down. Specifies what should occur immediately prior to the system shutting down.
        /// </summary>
        protected override void OnShutdown()
        {
            OnStop();
        }

    }
}
